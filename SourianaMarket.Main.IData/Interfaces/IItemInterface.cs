﻿using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.Dto.ItemDTO;
using SourianaMarket.Models.Commerce;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
    public interface IItemInterface
    {
        IEnumerable<ItemDto> GetItemsById(int id);
        //IEnumerable<ItemDto> GetWomanItem(int id);
        ItemDto GetItemById(int id);
        bool DeleteItem(int Id);
        bool EditAddItem(ItemDto itemSet);
        OperationResult SetItem(ItemDto model);

        List<ItemDto> Search(string term);
    }
}
