﻿using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.Dto.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
    public interface IBillInterface
    {
        IEnumerable<BillDto> GetAll(int userid);
        IEnumerable<BillDto> GetAllitem();
        OperationResult SetBill(BillDto model);
        bool Checkout(int userId);
    }
}
