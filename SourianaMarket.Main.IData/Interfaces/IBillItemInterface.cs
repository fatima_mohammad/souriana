﻿using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.Dto.BillItemDTO;
using SourianaMarket.Main.Dto.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
    public interface IBillItemInterface
    {
        IEnumerable<BillItemDto> GetAll(int userid);
        OperationResult SetBillItem(BillItemDto billitemDto);
        bool DeleteBillItem(int Id, int userid);
        bool DeleteAll(int userid);
    }
}
