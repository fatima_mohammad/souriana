﻿using SourianaMarket.Main.Dto.BrandDTO;
using SourianaMarket.Main.Dto.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
   public interface IBrandInterface
    {
        //IEnumerable<TreeItemDto> GetBrands(CityFilterCriteria cityFilterCriteria);
        //BrandDetailsDto GetBrand(int id);
        //OperationResult SetCity(NamedItemDto brandForm);
        //bool RemoveCity(int id);
        IEnumerable<BrandDto> GetAll();
        OperationResult SetBrand(BrandDto billDto);
        
        bool DeleteBrand(int Id);
        BrandDto GetBrandById(int id);
        bool EditAddBrand(BrandDto brandSet);
    }
}
