﻿using SourianaMarket.Main.Dto.CartDTO;
using SourianaMarket.Main.Dto.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
   public interface ICartInterface
    {
        IEnumerable<CartDto> GetAll();
        OperationResult SetCart(CartDto model);
        bool DeleteCart(int Id);
        bool DeleteAll();
    }
}
