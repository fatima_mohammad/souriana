﻿using SourianaMarket.Main.Dto.CategoryDTO;
using SourianaMarket.Main.Dto.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.IData.Interfaces
{
    public interface ICategoryInterface
    {
        List<CategoryDto> GetAll();
        OperationResult SetCategory(CategoryDto model);
        bool DeleteCategory(int Id);
       CategoryDto GetCategoryById(int id);
        bool EditAddCategory(CategoryDto categorySet);
        CategoryDto GetItemCategory(int itemId);
    }
}
