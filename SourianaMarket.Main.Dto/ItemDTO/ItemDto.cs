﻿using SourianaMarket.Models.Commerce;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.ItemDTO
{
   public class ItemDto
    {

        public int BrandCategoryId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Model { get; set; }


        public string ImageUrl { get; set; }
        //w
        public string Measure { get; set; }
        //w
        public string Type { get; set; }
        //C
        public string RAM { get; set; }
        public string VedioCard { get; set; }
        public string Screen { get; set; }
        public string CPU { get; set; }
        public string SSD { get; set; }
        public string HDD { get; set; }

        public string Brandname { get; set; }
        public string Categoryname { get; set; }
        public string CategoryType { get; set; }
        public int  CategoryId { get; set; }
        public int BrandId { get; set; }


    }
}
