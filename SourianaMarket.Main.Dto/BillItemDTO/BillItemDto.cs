﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.BillItemDTO
{
  public  class BillItemDto
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int BillId { get; set; }
        public int UserId { get; set; }
        public int Quantity { get; set; }
        public int OldPrice { get; set; }
        public DateTime Date { get; set; }
        public string ItemName { get; set; }

        public int ItemPrice { get; set; }
        public string ItemImageUrl { get; set; }
     
        public int Categid { get; set; }
    }
}
