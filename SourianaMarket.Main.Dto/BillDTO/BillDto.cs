﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.BillDTO
{
   public class BillDto
    {
        public int BillId { get; set; }
        public DateTime Date { get; set; }
        public int BillItemId { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemImageUrl { get; set; }
        public int Quantity { get; set; }
        public int Categid { get; set; }
        public int Userid { get; set; }
        public string Username { get; set; }

    }
}
