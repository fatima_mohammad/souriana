﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.CategoryDTO
{
   public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }    
        public string Description { get; set; }
        public string Type { get; set; }
        public string ImageUrl { get; set; }
    }
}
