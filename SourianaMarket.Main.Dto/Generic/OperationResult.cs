﻿using SourianaMarket.SharedKarnel.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.Generic
{
   public class OperationResult
    {
        public bool IsSuccess { get; set; }
        public OperationResultTypes ResultType { get; set; }
        public Exception ExceptionMessage { get; set; }
    }
}
