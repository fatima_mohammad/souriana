﻿using SourianaMarket.Models.Commerce;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.CartDTO
{
    public class CartDto
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice{ get; set; }
        public string ItemImageUrl { get; set; }
        public int Quantity { get; set; }
        public int Categid { get; set; }
       
    }
}
