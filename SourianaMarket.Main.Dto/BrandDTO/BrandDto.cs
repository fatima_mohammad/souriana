﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Dto.BrandDTO
{
    public class BrandDto
     {
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string ImageUrl { get; set; }
    }
}
