﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SourianaMarket.Models.User
{

    public class PlatformUser:IdentityUser<int>
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }    
        public string Info { get; set; }
        public bool IsValid { get; set; }


    }
}
