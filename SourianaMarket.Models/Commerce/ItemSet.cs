﻿using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
    public class ItemSet: BaseEntity
    {
        public int BrandCategoryId { get; set; }
        public BrandCategorySet BrandCategory { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Model { get; set; }
        public string ImageUrl { get; set; }
        //w
        public string Measure { get; set; }
        //w
        public string Type { get; set; }
        //C
        public string RAM { get; set; }
        public string VedioCard { get; set; }
        public string Screen { get; set; }
        public string CPU { get; set; }
        public string SSD { get; set; }
        public string HDD { get; set; }

        public ICollection<BillItemSet> BillItems { get; set; }
    }
}
