﻿//using Microsoft.AspNetCore.Http;
using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
   public class CategorySet: BaseEntity
    {
       
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string ImageUrl { get; set; }
        public ICollection<BrandCategorySet> BrandCategorys { get; set; }

    }
}
