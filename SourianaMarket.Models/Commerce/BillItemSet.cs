﻿using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
    public class BillItemSet: BaseEntity
    {
        public int ItemId   { get; set; }
        public int BillId   { get; set; }
        public int UserSetId   { get; set; }
        public ItemSet Item { get; set; }
        public BillSet Bill { get; set; }
        public UserSet UserSet { get; set; }
        public int Quantity { get; set; }
        public int OldPrice { get; set; }
    }
}
