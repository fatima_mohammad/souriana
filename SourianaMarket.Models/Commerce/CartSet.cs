﻿using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
   public class CartSet : BaseEntity
    {
        public int ItemId { get; set; }
        public ItemSet Item { get; set; }
        public int Quantity { get; set; }
        //public int PriceItem { get; set; }
    }
}
