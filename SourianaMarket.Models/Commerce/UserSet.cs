﻿using SourianaMarket.Models.Base;
using SourianaMarket.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
   public class UserSet: PlatformUser
    {
        public string Title { get; set; }
        public string Phone { get; set; }
        public ICollection<BillItemSet> BillItems { get; set; }

    }
}
