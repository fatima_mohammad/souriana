﻿using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
    public class BrandCategorySet: BaseEntity
    {
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public BrandSet Brand { get; set; }
        public CategorySet Category { get; set; }
        public ICollection<ItemSet> Items { get; set; }

    }
}
