﻿using SourianaMarket.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Commerce
{
  public  class BillSet: BaseEntity
    {
        public DateTime Date { get; set; }
        public bool State { get; set; }

        public ICollection<BillItemSet> BillItems { get; set; }

    }
}
