﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Models.Base
{
   public abstract  class  BaseEntity
    {
          public int Id { get; set; }
        public DateTimeOffset? DeleteAt { get; set; }
        public bool IsDeleted => !DeleteAt.HasValue; 
        public DateTimeOffset? AddedDate { get; set; }
        public int? AddedBy { get; set; }
    }
}
