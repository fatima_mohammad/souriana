﻿using SourianaMarket.Main.Dto.BillDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class BillViewModel
    {
        public IEnumerable<BillDto> BillDto { get; set; }
        public int BillId { get; set; }
        public DateTime Date { get; set; }
        public int BillItemId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public int UserId { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemImageUrl { get; set; }
        public int Categid { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }
    }
}
