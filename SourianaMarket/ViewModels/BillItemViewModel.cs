﻿using SourianaMarket.Main.Dto.BillItemDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class BillItemViewModel
    {
        public IEnumerable<BillItemDto> BillItemDto { get; set; }
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int BillId { get; set; }
        public int UserId { get; set; }
        public int Quantity { get; set; }
        public int OldPrice { get; set; }
        public DateTime Date { get; set; }
        public string ItemName { get; set; }

        public int ItemPrice { get; set; }
        public string ItemImageUrl { get; set; }

        public int Categid { get; set; }
    }
}
