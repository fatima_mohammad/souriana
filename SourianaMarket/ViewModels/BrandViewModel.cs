﻿using Microsoft.AspNetCore.Http;
using SourianaMarket.Main.Dto.BrandDTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class BrandViewModel
    {
        public IEnumerable<BrandDto> BrandDto { get; set; }
        public IFormFile File { get; set; }
        public int Id { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public BrandDto EditeBrand { get; set; }
       public string ImageUrl { get; set; }
        public bool IsAdmin { get; set; }
    }
}
