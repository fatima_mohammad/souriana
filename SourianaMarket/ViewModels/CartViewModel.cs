﻿using SourianaMarket.Main.Dto.CartDTO;
using SourianaMarket.Models.Commerce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class CartViewModel
    {
        public IEnumerable<CartDto> CartDto { get; set; }
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public int Categid { get; set; }

    }
}
