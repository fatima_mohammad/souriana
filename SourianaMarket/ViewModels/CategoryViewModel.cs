﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using SourianaMarket.Main.Dto.CategoryDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class CategoryViewModel
    {
        public List<CategoryDto> CategoryDto { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public CategoryDto EditeCategory { get; set; }

        public List<SelectListItem> TypeList { get; set; }
        public IFormFile File { get; set; }

        //public string Location { get; set; }
       public string ImageUrl { get; set; }
        public bool IsAdmin { get; set; }
    }
}
