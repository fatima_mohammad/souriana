﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class SignInViewModel
    {
        [Required(ErrorMessage = "The user name  is required")]

        public string UserName { get; set; }
        [Required(ErrorMessage = "The Password  is required")]

        public string Password { get; set; }
    }
}
