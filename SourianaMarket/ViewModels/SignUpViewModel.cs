﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class SignUpViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public double DollarPeerHour { get; set; }
        public string UserName { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "not matching with password")]
        public string ConfermPassword { get; set; }
        public string ImageUrl { get; set; }
        public string Info { get; set; }
        [RegularExpression("^([0-9 .&'-]+)$", ErrorMessage = "Characters are not allowed")]
        public string PhoneNumber { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public bool RemeberMe { get; set; }

    }
}
