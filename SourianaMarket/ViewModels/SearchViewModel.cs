﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using SourianaMarket.Main.Dto.BrandDTO;
using SourianaMarket.Main.Dto.ItemDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.ViewModels
{
    public class SearchViewModel
    {
        public List<ItemDto>Items { get; set; }
        public List<SelectListItem> Company { get; set; }
        public List<SelectListItem> MeasList { get; set; }
        public List<SelectListItem> TList { get; set; }
        public IFormFile File { get; set; }
        public int Id { get; set; }
        public int BrandCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Model { get; set; }
        public string ImageUrl { get; set; }
        //w
        public int Quantity { get; set; }

        public string Measure { get; set; }
        //w
        public string Type { get; set; }
        //C
        public string RAM { get; set; }
        public string VedioCard { get; set; }
        public string Screen { get; set; }
        public string CPU { get; set; }
        public string SSD { get; set; }
        public string HDD { get; set; }
        public string Brandname { get; set; }
        public string Categoryname { get; set; }
        public string CategoryType { get; set; }
        public List<BrandDto> Brands { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public int ItemId { get; set; }

        public ItemDto EditeItem { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsUser { get; set; }
    }
}
