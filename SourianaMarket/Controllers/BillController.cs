﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SourianaMarket.Main.Data.Repositories;
using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.Security;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    public class BillController : Controller
    {
        public IBillInterface BillInterface { get; set; }
        public IAccountRepository AccountRepository { get; set; }
        public BillController(IBillInterface billInterface, IAccountRepository accountRepository)
        {
            BillInterface = billInterface;
            AccountRepository = accountRepository;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var userid = AccountRepository.GetUserId();
            var vm = new BillViewModel()
            {
                BillDto = BillInterface.GetAll(userid),
                IsAdmin = this.User.IsInRole(RoleName.Admin.ToString())
            };
            return View(vm);
        }
        public IActionResult Details()
        {
            var vm = new BillViewModel()
            {
                BillDto = BillInterface.GetAllitem()

            };
            return View(vm);
        }

        private BillViewModel FillViewModel(BillViewModel model)
        {

            return new BillViewModel()
            {
                BillId = model.BillId,
                Date = model.Date

                //ImageUrl = model.ImageUrl
            };

        }
        [HttpPost]
        public IActionResult SaveBillItemInfo(BillViewModel model)
        {

            if (!ModelState.IsValid)
            {
                var viewModel = FillViewModel(model);
                ViewBag.error = "Data incorrect";
                return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
            }


            var result = BillInterface.SetBill(new BillDto()
            {
            });
            if (result.ResultType == OperationResultTypes.Success)
            {
                //return json(result);
                return RedirectToAction(nameof(CartController.Index),
                    nameof(CartController).GetActionName());
            }
            ModelState.AddModelError("", "failed to update date");
            //var vm = FillViewModel(model);
            return RedirectToAction(nameof(CartController.Index),
                       nameof(CartController).GetActionName());
        }


        [Authorize]
        public IActionResult CheckOut()
        {

            var result = BillInterface.Checkout(AccountRepository.GetUserId());
            if (result)
            {
                //return json(result);
                return RedirectToAction(nameof(BillController.Index),
                    nameof(BillController).GetActionName());
            }
            ModelState.AddModelError("", "failed to update date");
            //var vm = FillViewModel(model);
            return RedirectToAction(nameof(CartController.Index),
                       nameof(CartController).GetActionName());
        }

    }
}
