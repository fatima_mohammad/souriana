﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SourianaMarket.Main.Dto.BrandDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    public class BrandController : Controller
    {
        public IBrandInterface BrandInterface { get; set; }
        public IHostingEnvironment Hosting { get; set; }

        public BrandController(IBrandInterface brandInterface, IHostingEnvironment hosting)
        {
            BrandInterface = brandInterface;
            Hosting = hosting;
        }
        public IActionResult Index()
        {
            //var brands = BrandInterface.GetAll();
            //return View(brands);
            var vm = new BrandViewModel()
            {
                BrandDto = BrandInterface.GetAll(),
                IsAdmin = this.User.IsInRole(RoleName.Admin.ToString())
            };
            return View(vm);

        }

        [HttpPost]
        public IActionResult SaveBrandInfo(BrandViewModel model)
        {
            string PathDB = string.Empty;

            try
            {
                if (model.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (model.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            model.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
               else
                {
                    PathDB = "/ForBrand/images/placeb.png";
                }
            }
            catch (Exception ex )
            {

                throw ex;
            }
           
           
                if (!ModelState.IsValid)
            {
                var viewModel = FillViewModel(model);
                ViewBag.error = "Data incorrect";
                return RedirectToAction(nameof(BrandController.Index),nameof(BrandController).GetActionName());
            }
        
                var result = BrandInterface.SetBrand(new BrandDto()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    Location = model.Location,
                    ImageUrl = PathDB

                });
                if (result.ResultType == OperationResultTypes.Success)
                {
                    //return json(result);
                    return RedirectToAction(nameof(BrandController.Index),
                        nameof(BrandController).GetActionName());
                }
            
            
            
         

            ModelState.AddModelError("", "failed to update date");
            //var vm = FillViewModel(model);
            return View();
        }
      
        private BrandViewModel FillViewModel(BrandViewModel model)
        {
           
                return new BrandViewModel()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    Location = model.Location,
                    //ImageUrl = model.ImageUrl
                };
             
            }
    
        public IActionResult RemoveBrand(int id)
        {

            bool DelBrand = BrandInterface.DeleteBrand(id);
            if (DelBrand)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(BrandController.Index), nameof(BrandController).GetActionName());
            }
            return RedirectToAction(nameof(BrandController.Index), nameof(BrandController).GetActionName());
            //Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //return Json("Please retry field to remove country !");
        }
        public IActionResult Edit(int id)
        {

            var brand = BrandInterface.GetBrandById(id);
            var vm = new BrandViewModel()
            {
                Name =brand.Name,
                Description=brand.Description,
                Location=brand.Location,
                ImageUrl=brand.ImageUrl
            };
            return View(vm);
        }

        [HttpPost]
        public IActionResult EditBrand(BrandViewModel model)
        {

            string PathDB = string.Empty;

            try
            {
                if (model.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (model.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            model.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            bool setBrand = BrandInterface.EditAddBrand(new BrandDto()
            {
                Name = model.EditeBrand.Name,
                Description = model.EditeBrand.Description,
                Location = model.EditeBrand.Location,
                 ImageUrl= PathDB,

                Id = model.EditeBrand.Id,
            });
            if (setBrand)
            {
                return RedirectToAction(nameof(BrandController.Index),
                   nameof(BrandController).GetActionName());
            }

            return RedirectToAction(nameof(BrandController.Index),
                     nameof(BrandController).GetActionName());
        }

       
    }
}
