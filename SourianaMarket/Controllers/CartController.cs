﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SourianaMarket.Main.Dto.CartDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        public ICartInterface CartInterface { get; set; }
        public CartController(ICartInterface cartInterface)
        {
            CartInterface = cartInterface;
        }
        public IActionResult Index()
        {
            var vm = new CartViewModel()
            {
                CartDto = CartInterface.GetAll()
              
            };
            return View(vm);
        }

      
        private CartViewModel FillViewModel(CartViewModel model)
        {

            return new CartViewModel()
            {
                Id = model.Id,
              ItemId=model.ItemId,
                Quantity = model.Quantity,
               
            };

        }
        public IActionResult RemoveCart(int id)
        {

            bool DelCart = CartInterface.DeleteCart(id);
            if (DelCart)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
            }
            return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
            //Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //return Json("Please retry field to remove country !");
        }
        public IActionResult DeleteAll()
        {
           

            bool DeletAllCart = CartInterface.DeleteAll();
            if(DeletAllCart)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
            }
            return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
        }
    }
}
