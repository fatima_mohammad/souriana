﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SourianaMarket.Main.Data.Repositories;
using SourianaMarket.Main.Dto.CategoryDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{

    public class CategoryController : Controller
    {

        public ICategoryInterface CategoryInterface { get; set; }
        public IHostingEnvironment Hosting { get; set; }
        public List<String> ss { get; set; }
       
        public CategoryController(ICategoryInterface categoryInterface, IHostingEnvironment hosting)
        {

            CategoryInterface = categoryInterface;
            Hosting = hosting;
            ss = new List<string>();
            ss.Add("Wearing");
            ss.Add("Electronic");

        }

        public IActionResult Index()
        {
            //var brands = BrandInterface.GetAll();
            //return View(brands);
         
           
            var vm = new CategoryViewModel()
            {
                CategoryDto = CategoryInterface.GetAll(),
                IsAdmin = this.User.IsInRole(RoleName.Admin.ToString()),
                TypeList = ss.Select(oo => new SelectListItem()
                {
                    Value= oo.ToString(),
                    Text =oo.ToString()
                }).ToList(),
            };
            return View(vm);

        }

        [HttpPost]
        public IActionResult SaveCategoryInfo(CategoryViewModel model)
        {


            string PathDB = string.Empty;

            try
            {
                if (model.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (model.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            model.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
                else
                {
                    PathDB = "/ForBrand/images/ppp.jpg";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


            if (!ModelState.IsValid)
            {
                var viewModel = FillViewModel(model);
                ViewBag.error = "Data incorrect";
                return RedirectToAction(nameof(CategoryController.Index), nameof(CategoryController).GetActionName());
            }

            var result = CategoryInterface.SetCategory(new CategoryDto()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                ImageUrl = PathDB

            });
            if (result.ResultType == OperationResultTypes.Success)
            {
                //return json(result);
                return RedirectToAction(nameof(CategoryController.Index),
                    nameof(CategoryController).GetActionName());
            }

            ModelState.AddModelError("", "failed to update date");
            //var vm = FillViewModel(model);
            return View();
        }
        private CategoryViewModel FillViewModel(CategoryViewModel model)
        {

            return new CategoryViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                //ImageUrl = model.ImageUrl
            };

        }

        public IActionResult RemoveCategory(int id)
        {

            bool DelCategory = CategoryInterface.DeleteCategory(id);
            if (DelCategory)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(CategoryController.Index), nameof(CategoryController).GetActionName());
            }
            return RedirectToAction(nameof(CategoryController.Index), nameof(CategoryController).GetActionName());
            //Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //return Json("Please retry field to remove country !");
        }

        [HttpPost]
        public IActionResult EditCategory(CategoryViewModel model)
        {

            string PathDB = string.Empty;

            try
            {
                if (model.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (model.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            model.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            bool setCategory = CategoryInterface.EditAddCategory(new CategoryDto()
            {
                Name = model.EditeCategory.Name,
                Description = model.EditeCategory.Description,
                Type = model.EditeCategory.Type,
                ImageUrl = PathDB,

                Id = model.EditeCategory.Id
            });
            if (setCategory)
            {
                return RedirectToAction(nameof(CategoryController.Index),
                   nameof(CategoryController).GetActionName());
            }

            return RedirectToAction(nameof(CategoryController.Index),
                     nameof(CategoryController).GetActionName());
        }


    }
}
