﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SourianaMarket.Main.Data.Repositories;
using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Security;
//using SourianaMarket.Main.Dto.BillItemDTO;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    public class BillItemController : Controller
    {
        public IBillItemInterface BillItemInterface { get; set; }
        public IAccountRepository AccountRepository { get; set; }
        public BillItemController(IBillItemInterface billitemtInterface, IAccountRepository accountRepository)
        {
            BillItemInterface = billitemtInterface;
            AccountRepository = accountRepository;  
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var userid = AccountRepository.GetUserId();
            var vm = new BillItemViewModel()
            {
                BillItemDto = BillItemInterface.GetAll(userid)

            };
            return View(vm);
        }
        private BillViewModel FillViewModel(BillViewModel model)
        {

            return new BillViewModel()
            {
                BillId = model.BillId,
                ItemId = model.ItemId,
                UserId = model.UserId,
                Quantity = model.Quantity,
                ItemPrice = model.ItemPrice
                //ImageUrl = model.ImageUrl
            };

        }
        public IActionResult RemoveBillItem(int id)
        {

            var userid = AccountRepository.GetUserId();
            bool DelBillItem = BillItemInterface.DeleteBillItem(id, userid);
            if (DelBillItem)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(BillItemController.Index), nameof(BillItemController).GetActionName());
            }
            return RedirectToAction(nameof(BillItemController.Index), nameof(BillItemController).GetActionName());
            //Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //return Json("Please retry field to remove country !");
        }
        public IActionResult DeleteAll()
        {
            var userid = AccountRepository.GetUserId();
            bool DeletAllBillItem = BillItemInterface.DeleteAll(userid);
            if (DeletAllBillItem)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(BillItemController.Index), nameof(BillItemController).GetActionName());
            }
            return RedirectToAction(nameof(BillItemController.Index), nameof(BillItemController).GetActionName());
        }
        //    [HttpPost]
        //    public IActionResult SaveBillItemInfo(BillViewModel model)
        //    {
        //        string PathDB = string.Empty;

        //        if (!ModelState.IsValid)
        //        {
        //            var viewModel = FillViewModel(model);
        //            ViewBag.error = "Data incorrect";
        //            return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
        //        }
        //        var result = BillItemInterface.SetBillItem(new BillDto()
        //        {
        //            BillId = model.BillId,
        //            ItemId = model.ItemId,
        //            Userid = model.UserId,
        //            Quantity = model.Quantity,
        //            ItemPrice = model.ItemPrice
        //        });
        //        if (result.ResultType == OperationResultTypes.Success)
        //        {
        //            //return json(result);
        //            return RedirectToAction(nameof(CartController.Index),
        //                nameof(CartController).GetActionName());
        //        }
        //        ModelState.AddModelError("", "failed to update date");
        //        //var vm = FillViewModel(model);
        //        return RedirectToAction(nameof(CartController.Index),
        //                   nameof(CartController).GetActionName());
        //    }
        //}

        
    }
}
