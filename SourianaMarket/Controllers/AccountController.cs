﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SourianaMarket.Security;
using SourianaMarket.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    public class AccountController : Controller
    {
        public IAccountRepository AccountRepository { get; set; }
        public AccountController(IAccountRepository accountRepository)
        {
            AccountRepository = accountRepository;
        }
        // GET: /<controller>/
        public IActionResult Login()   
        {
            return View(new SignInViewModel());
        }
        public IActionResult SignUp()
        {
            return View(new SignUpViewModel());
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(SignInViewModel signInViewModel,string returnUrl)
        {
            if (!ModelState.IsValid)
            {


                return View();
            }
            //    var sd = await AccountRepository.CreatRole();
            if (!string.IsNullOrEmpty(signInViewModel.UserName) && !string.IsNullOrEmpty(signInViewModel.Password))
            {
                var result = await AccountRepository.HttpLogin(new AccountDto()
                {
                    UserName = signInViewModel.UserName,
                    Password = signInViewModel.Password
                });
                if (result.Id != 0)
                {
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home", new { id = result.Id });
                    }

                }
                ModelState.AddModelError("ErrorSign", "Name or Password UnCorrect");
                return View();


            }

            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpViewModel signUpViewModel)
        {
            if (!ModelState.IsValid)
            {

                ModelState.AddModelError("errorModel", "Characters are not allowed.");

                return View();
            }
            AccountDto accountDto = new AccountDto()
            {
                FirstName = signUpViewModel.FullName,
                Email = signUpViewModel.Email,
                ImageUrl = signUpViewModel.ImageUrl,
                Info = signUpViewModel.Info,
                PhoneNumber = signUpViewModel.PhoneNumber,
                RoleName = signUpViewModel.RoleName,
                UserName = signUpViewModel.UserName,
                Password = signUpViewModel.Password,
                RemeberMe = signUpViewModel.RemeberMe,
                RoleId = 2,
            };


            var result = await AccountRepository.AciotnAccount(accountDto);
            var signin = await AccountRepository.HttpLogin(new AccountDto()
            {
                UserName = signUpViewModel.UserName,
                Password = signUpViewModel.Password
            });

            return RedirectToAction("Index", "Home");

        }
    }
}
