﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SourianaMarket.Main.Dto.BillItemDTO;
using SourianaMarket.Main.Dto.CartDTO;
using SourianaMarket.Main.Dto.ItemDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Security;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.Util;
using SourianaMarket.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SourianaMarket.Controllers
{
    public class ItemController : Controller
    {
        public  IBrandInterface BrandInterface { get; set; }
        public IBillItemInterface BillItemInterface { get; set; }
        public List<String> MeasList { get; set; }
        public List<String> TList { get; set; }
        public IItemInterface ItemInterface { get; set; }
        public ICategoryInterface CategoryInterface { get; set; }
        public IAccountRepository AccountRepository { get; set; }
        public IHostingEnvironment Hosting { get; set; }
        public ItemController(IBillItemInterface billitemInterface,IItemInterface itemInterface,
            IBrandInterface brandInterface, ICategoryInterface categoryInterface, IHostingEnvironment hosting
            , IAccountRepository accountInterface)
        {

            BillItemInterface = billitemInterface;
            ItemInterface = itemInterface;
            BrandInterface = brandInterface;
            AccountRepository = accountInterface;
            CategoryInterface = categoryInterface;
            Hosting = hosting;

            MeasList = new List<string>();
            TList = new List<string>();
            MeasList.Add("Max");
            MeasList.Add("Mid");
            MeasList.Add("Min");
            TList.Add("Man");
            TList.Add("Woman");
            TList.Add("KIDS");

        }
        [Authorize]
        public IActionResult Index(int id)
        {
            var vm = new ItemViewModel()
            {
                ItemDto = ItemInterface.GetItemsById(id),
                Company= BrandInterface.GetAll().Select(oo=>new SelectListItem()
                {
                    Value=oo.Id.ToString(),
                    Text=oo.Name
                }).ToList(),
               MeasList  = MeasList.Select(oo => new SelectListItem()
               {
                   Value = oo.ToString(),
                   Text = oo.ToString()
               }).ToList(),
                TList = TList.Select(oo => new SelectListItem()
                {
                    Value = oo.ToString(),
                    Text = oo.ToString()
                }).ToList(),
                CategoryId =id,
                CategoryType= CategoryInterface.GetCategoryById(id).Type,
                IsAdmin = this.User.IsInRole(RoleName.Admin.ToString())
                ,
                IsUser = this.User.IsInRole(RoleName.User.ToString())
                // Brands = BrandInterface.GetAll(),
                // BrandID=BrandInterface.
            };
            return View(vm);
        }
     
      
        public IActionResult Details(int id)
        {
          var itemdto=  ItemInterface.GetItemById(id);
            var vm = new ItemViewModel()
            { 
                Id=itemdto.Id,
                Name = itemdto.Name,
                Description = itemdto.Description,
                ImageUrl=itemdto.ImageUrl,
                Price = itemdto.Price,
                Measure = itemdto.Measure,
                Type = itemdto.Type,
                RAM = itemdto.RAM,
                VedioCard = itemdto.VedioCard,
                Screen = itemdto.Screen,
                CPU = itemdto.CPU,
                SSD = itemdto.SSD,
                HDD = itemdto.HDD,
                Brandname = itemdto.Brandname,
                Categoryname = itemdto.Categoryname,
                CategoryType = itemdto.CategoryType
            };
            return View(vm);
        }
       
        public IActionResult Delete(int id)
        {

            var itemdto = ItemInterface.GetItemById(id);
            var vm = new ItemViewModel()
            {
                Id = itemdto.Id,
                Name = itemdto.Name,
                Description = itemdto.Description,
                ImageUrl = itemdto.ImageUrl,
                Price = itemdto.Price,
                Measure = itemdto.Measure,
                Type = itemdto.Type,
                RAM = itemdto.RAM,
                VedioCard = itemdto.VedioCard,
                Screen = itemdto.Screen,
                CPU = itemdto.CPU,
                SSD = itemdto.SSD,
                HDD = itemdto.HDD,
                Brandname = itemdto.Brandname,
                Categoryname = itemdto.Categoryname,
                CategoryType = itemdto.CategoryType
            };
            return View(vm);
        }
        [HttpPost]
        public IActionResult ConfirmDelete(int  id)
        {
            var catego = CategoryInterface.GetItemCategory(id);
            bool DelItem = ItemInterface.DeleteItem(id);
            if (DelItem)
            {
                //return Json("Remove Country successfully");
                return RedirectToAction(nameof(ItemController.Index), nameof(ItemController).GetActionName(), new {id=catego.Id});
            }
          
            return RedirectToAction(nameof(ItemController.Index), nameof(ItemController).GetActionName(), new { id = catego.Id });
            //Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //return Json("Please retry field to remove country !");
        }

        public IActionResult Edit(int id)
        {
          
            var item = ItemInterface.GetItemById(id);
            var vm = new ItemViewModel()
            {
                Name = item.Name,
                Description = item.Description,
                Price = item.Price,
                Model=item.Model,
                ImageUrl=item.ImageUrl,
                Measure = item.Measure,
                Type = item.Type,
                RAM = item.RAM,
                VedioCard = item.VedioCard,
                Screen = item.Screen,
                CPU = item.CPU,
                SSD = item.SSD,
                HDD = item.HDD,
                Id=item.Id,
                BrandCategoryId = item.BrandCategoryId
                ,BrandId=item.BrandId,
                  Company = BrandInterface.GetAll().Select(oo => new SelectListItem()
                  {
                      Value = oo.Id.ToString(),
                      Text = oo.Name
                  }).ToList(),
                MeasList = MeasList.Select(oo => new SelectListItem()
                {
                    Value = oo.ToString(),
                    Text = oo.ToString()
                }).ToList(),
                TList = TList.Select(oo => new SelectListItem()
                {
                    Value = oo.ToString(),
                    Text = oo.ToString()
                }).ToList(),
                CategoryId = item.CategoryId,
            CategoryType = CategoryInterface.GetCategoryById(item.CategoryId).Type
                //,ImageUrl=brand.ImageUrl
            };
            return View(vm);
        }

        public IActionResult EditItem(ItemViewModel model)
        {

            string PathDB = string.Empty;

            try
            {
                if (model.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (model.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(model.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            model.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            var catego = CategoryInterface.GetItemCategory(model.EditeItem.Id);
            bool setItem = ItemInterface.EditAddItem(new ItemDto()
            {
                Name = model.EditeItem.Name,
                Description = model.EditeItem.Description,
                Price = model.EditeItem.Price,
                Model = model.EditeItem.Model,
                ImageUrl = PathDB,
                Measure = model.EditeItem.Measure,
                Type = model.EditeItem.Type,
                RAM = model.EditeItem.RAM,
                VedioCard = model.EditeItem.VedioCard,
                Screen = model.EditeItem.Screen,
                CPU = model.EditeItem.CPU,
                SSD = model.EditeItem.SSD,
                HDD = model.EditeItem.HDD,
                Id = model.EditeItem.Id,
                CategoryId=catego.Id,
                BrandId=model.BrandId
            });
            if (setItem)
            {
                return RedirectToAction(nameof(ItemController.Index),
                   nameof(ItemController).GetActionName(), new { id = catego.Id });
            }

            return RedirectToAction(nameof(ItemController.Index),
                     nameof(ItemController).GetActionName(), new { id = catego.Id });
        }


        [HttpPost]  
        public IActionResult SaveCartInfo(ItemViewModel model)
        {

          

            if (!ModelState.IsValid)
            {
                ViewBag.error = "Data incorrect";
                return RedirectToAction(nameof(CartController.Index), nameof(CartController).GetActionName());
            }

            //if all user bills true Creat new bill else add item to false bill 
            //set to bill 
            var vm = new ItemViewModel()
            {
              
                IsAdmin = this.User.IsInRole(RoleName.Admin.ToString()),
                IsUser = this.User.IsInRole(RoleName.User.ToString())
            };
           if(vm.IsAdmin==true||vm.IsUser==true)
            {
                var result = BillItemInterface.SetBillItem(new BillItemDto()
                {
                    ItemId = model.ItemId,
                    Quantity = model.Quantity,
                    OldPrice = model.Price,
                    UserId = AccountRepository.GetUserId()
                });
                if (result.ResultType == OperationResultTypes.Success)
                {
                    //return json(result);
                    return RedirectToAction(nameof(ItemController.Index),
                        nameof(ItemController).GetActionName(), new { id = model.Id });
                }
                ModelState.AddModelError("", "failed to update date");
                //var vm = FillViewModel(model);
                return View();
            }
            else
            {
                return RedirectToAction(nameof(AccountController.SignUp),
                        nameof(AccountController).GetActionName());
            }
           
        }
        [HttpPost]
        public IActionResult SetItemInfo(ItemViewModel vm)
        {

            string PathDB = string.Empty;

            try
            {
                if (vm.File != null)
                {

                    var fileName = string.Empty;

                    var files = HttpContext.Request.Form.Files;

                    var newFileName = string.Empty;


                    if (vm.File.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(vm.File.ContentDisposition).FileName.Trim('"');
                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());
                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);
                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;
                        // Combines two strings into a path.
                        fileName = Path.Combine(Hosting.WebRootPath, "ForBrand", newFileName);
                        fileName.Replace('\\', Path.DirectorySeparatorChar);
                        // if you want to store path of folder in database
                        PathDB = "/ForBrand/" + newFileName;
                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            vm.File.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }
                else
                {
                    PathDB = "/ForBrand/images/iconitm.png";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


            //var catego = CategoryInterface.GetItemCategory(vm.Id);
            if (!ModelState.IsValid)
            {
                var viewModel = FillViewModel(vm);
                ViewBag.error = "Data incorrect";
                return RedirectToAction(nameof(ItemController.Index), nameof(ItemController).GetActionName(),new { id = vm.CategoryId });
            }

            var result = ItemInterface.SetItem(new ItemDto()
            {
                Id = vm.Id,
                
                Name = vm.Name,
                Description = vm.Description,
                Type = vm.Type,
                Price = vm.Price,
                Model = vm.Model,
                ImageUrl = PathDB,
                Measure = vm.Measure,
             
                RAM = vm.RAM,
                VedioCard = vm.VedioCard,
                Screen = vm.Screen,
                CPU = vm.CPU,
                SSD = vm.SSD,
                HDD = vm.HDD
                ,CategoryId=vm.CategoryId,
                BrandId=vm.BrandId
            });
            if (result.ResultType == OperationResultTypes.Success)
            {
                //return json(result);
                return RedirectToAction(nameof(ItemController.Index),
                    nameof(ItemController).GetActionName(), new { id = vm.CategoryId });
            }

            ModelState.AddModelError("", "failed to update date");
            //var vm = FillViewModel(model);
            return View();
        }
        private ItemViewModel FillViewModel(ItemViewModel model)
        {
            return new ItemViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                //ImageUrl = model.ImageUrl
            };
        }

        [HttpPost]
        public IActionResult Search(string term)
        {
           
            var result = ItemInterface.Search(term);
            SearchViewModel vm = new SearchViewModel()
            {
                Items = result
            };
            return View(vm);
            
        }
    }
}
