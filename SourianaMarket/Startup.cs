﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SourianaMarket.Main.Data.Repositories;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Security;
using SourianaMarket.Models.User;
using SourianaMarket.Security;
using SourianaMarket.SqlServer.Database;

namespace SourianaMarket
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IConfiguration Configuration { get; }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

     
        public String GetConnectionString(string CollectionStringKey)
        {
            var ConnectionString = Configuration.GetSection("ConnectionStrings");
            return ConnectionString.GetValue<String>(CollectionStringKey);
        }

        public void ConfigureServices(IServiceCollection services)
        {
           
                services.AddDbContext<SourianaMarketContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("MySourianaMarketDB"));
                });
                services.AddIdentity<PlatformUser, PlatformRole>(options =>
                {
                    options.User.RequireUniqueEmail = false;
                })
              .AddEntityFrameworkStores<SourianaMarketContext>()
              .AddDefaultTokenProviders();
                services.Configure<IdentityOptions>(option =>
                {

                    option.Password.RequireDigit = false;
                    option.Password.RequireLowercase = false;
                    option.Password.RequireNonAlphanumeric = false;
                    option.Password.RequireUppercase = false;
                    option.Password.RequiredLength = 3;
                    option.Password.RequiredUniqueChars = 1;
                });
                services.ConfigureApplicationCookie(options =>
                {
                    options.Cookie.HttpOnly = true;
                    //options.ExpireTimeSpan = TimeSpan.FromSeconds(10);
                    options.LoginPath = "/Account/Login/";
                    options.AccessDeniedPath = "/User/SignIn/";

                    //options.SlidingExpiration = true;
                });

                services.Configure<CookiePolicyOptions>(options =>
                {
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });

                services.AddScoped<IBrandInterface, BrandInterface>();
            services.AddScoped<ICategoryInterface, CategoryInterface>();
            services.AddScoped<IBrandCategoryInterface, BrandCategoryInterface>();
            services.AddScoped<IItemInterface, ItemInterface>();
            services.AddScoped<IBillInterface, BillInterface>();
            services.AddScoped<IBillItemInterface, BillItemInterface>();
            services.AddScoped<IUserInterface, UserInterface>();
            services.AddScoped<ICartInterface, CartInterface>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped(sp => sp.GetRequiredService<IHttpContextAccessor>().HttpContext);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                //template: "{controller=User}/{action=SignIn}/{id?}");
               template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
