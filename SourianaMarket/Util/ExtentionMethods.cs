﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.Util
{
    public static class ExtentionMethods
    {
        public static string GetActionName(this string actionName)
        {
            return actionName.Replace("Controller", String.Empty);
        }
    }
}
