﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourianaMarket.Util
{
    public static class ViewPaths
    {
        public static string ScriptPartial = "~/Views/Shared/_ScriptsPartial.cshtml";
        public static string LinksPartial = "~/Views/Shared/_LinksPartial.cshtml";
        public static string HeaderPartial = "~/Views/Shared/_HeaderPartial.cshtml";
        //public static string FooterPartial = "~/Views/Shared/_LinksPartial.cshtml";

    }
}
