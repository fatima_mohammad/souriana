﻿function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.fileEdite').closest("form").find(".imageEdit").attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".fileEdite").change(function () {
    console.log("hi");
    readURL(this);
})