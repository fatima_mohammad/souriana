﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SourianaMarket.SqlServer.Migrations
{
    public partial class adduserSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BillItems_Users_UserId",
                table: "BillItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "AddedBy",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "AddedDate",
                table: "Users");

            migrationBuilder.EnsureSchema(
                name: "Security");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "PlatformUser",
                newSchema: "Security");

            migrationBuilder.RenameColumn(
                name: "DeleteAt",
                schema: "Security",
                table: "PlatformUser",
                newName: "LockoutEnd");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "Security",
                table: "PlatformUser",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AccessFailedCount",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ConcurrencyStamp",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "EmailConfirmed",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Info",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsValid",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "LockoutEnabled",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedEmail",
                schema: "Security",
                table: "PlatformUser",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedUserName",
                schema: "Security",
                table: "PlatformUser",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PasswordHash",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PhoneNumberConfirmed",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SecurityStamp",
                schema: "Security",
                table: "PlatformUser",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TwoFactorEnabled",
                schema: "Security",
                table: "PlatformUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                schema: "Security",
                table: "PlatformUser",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlatformUser",
                schema: "Security",
                table: "PlatformUser",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "PlatformRole",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformRole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlatformUserCLaim",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformUserCLaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlatformUserCLaim_PlatformUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "PlatformUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatformUserLogin",
                schema: "Security",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformUserLogin", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_PlatformUserLogin_PlatformUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "PlatformUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatformUserToken",
                schema: "Security",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformUserToken", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_PlatformUserToken_PlatformUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "PlatformUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatformRoleClim",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformRoleClim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlatformRoleClim_PlatformRole_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Security",
                        principalTable: "PlatformRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatformUserRole",
                schema: "Security",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformUserRole", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_PlatformUserRole_PlatformRole_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Security",
                        principalTable: "PlatformRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlatformUserRole_PlatformUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "PlatformUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Carts_ItemId",
                table: "Carts",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "Security",
                table: "PlatformUser",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "Security",
                table: "PlatformUser",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "Security",
                table: "PlatformRole",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformRoleClim_RoleId",
                schema: "Security",
                table: "PlatformRoleClim",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformUserCLaim_UserId",
                schema: "Security",
                table: "PlatformUserCLaim",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformUserLogin_UserId",
                schema: "Security",
                table: "PlatformUserLogin",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatformUserRole_RoleId",
                schema: "Security",
                table: "PlatformUserRole",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_BillItems_PlatformUser_UserId",
                table: "BillItems",
                column: "UserId",
                principalSchema: "Security",
                principalTable: "PlatformUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Carts_Items_ItemId",
                table: "Carts",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BillItems_PlatformUser_UserId",
                table: "BillItems");

            migrationBuilder.DropForeignKey(
                name: "FK_Carts_Items_ItemId",
                table: "Carts");

            migrationBuilder.DropTable(
                name: "PlatformRoleClim",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "PlatformUserCLaim",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "PlatformUserLogin",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "PlatformUserRole",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "PlatformUserToken",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "PlatformRole",
                schema: "Security");

            migrationBuilder.DropIndex(
                name: "IX_Carts_ItemId",
                table: "Carts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlatformUser",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropIndex(
                name: "EmailIndex",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropIndex(
                name: "UserNameIndex",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "AccessFailedCount",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "ConcurrencyStamp",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "EmailConfirmed",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "Info",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "IsValid",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "LockoutEnabled",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "NormalizedEmail",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "NormalizedUserName",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "PasswordHash",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "PhoneNumberConfirmed",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "SecurityStamp",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "TwoFactorEnabled",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.DropColumn(
                name: "UserName",
                schema: "Security",
                table: "PlatformUser");

            migrationBuilder.RenameTable(
                name: "PlatformUser",
                schema: "Security",
                newName: "Users");

            migrationBuilder.RenameColumn(
                name: "LockoutEnd",
                table: "Users",
                newName: "DeleteAt");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddedBy",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "AddedDate",
                table: "Users",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BillItems_Users_UserId",
                table: "BillItems",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
