﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SourianaMarket.SqlServer.Migrations
{
    public partial class initialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BillItems_PlatformUser_UserId",
                table: "BillItems");

            migrationBuilder.DropColumn(
                name: "DeliverType",
                table: "Bills");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "BillItems",
                newName: "UserSetId");

            migrationBuilder.RenameIndex(
                name: "IX_BillItems_UserId",
                table: "BillItems",
                newName: "IX_BillItems_UserSetId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "Bills",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "State",
                table: "Bills",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OldPrice",
                table: "BillItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_BillItems_PlatformUser_UserSetId",
                table: "BillItems",
                column: "UserSetId",
                principalSchema: "Security",
                principalTable: "PlatformUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BillItems_PlatformUser_UserSetId",
                table: "BillItems");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Bills");

            migrationBuilder.DropColumn(
                name: "OldPrice",
                table: "BillItems");

            migrationBuilder.RenameColumn(
                name: "UserSetId",
                table: "BillItems",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_BillItems_UserSetId",
                table: "BillItems",
                newName: "IX_BillItems_UserId");

            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "Bills",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "DeliverType",
                table: "Bills",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BillItems_PlatformUser_UserId",
                table: "BillItems",
                column: "UserId",
                principalSchema: "Security",
                principalTable: "PlatformUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
