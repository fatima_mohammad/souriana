﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SourianaMarket.Models.Commerce;
using SourianaMarket.Models.Security;
using SourianaMarket.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.SqlServer.Database
{
    public class SourianaMarketContext : IdentityDbContext<PlatformUser, PlatformRole, int,
                                    PlatformUserCLaim, PlatformUserRole,
                                    PlatformUserLogin, PlatformRoleClim, PlatformUserToken>
    {

        public DbSet<BrandSet> Brands { get; set; }
        public DbSet<CategorySet> Categorys { get; set; }
        public DbSet<BrandCategorySet> BrandCategorys { get; set; }
        public DbSet<ItemSet> Items { get; set; }
        public DbSet<BillSet> Bills { get; set; }
        public DbSet<BillItemSet> BillItems { get; set; }
        public DbSet<UserSet> Users { get; set; }
        public DbSet<CartSet> Carts { get; set; }

        public SourianaMarketContext(DbContextOptions<SourianaMarketContext> options)
            : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); 


            #region Change security table names
            modelBuilder.Entity<PlatformUser>().ToTable("PlatformUser", "Security");
            modelBuilder.Entity<PlatformRole>().ToTable("PlatformRole", "Security");
            modelBuilder.Entity<PlatformUserCLaim>().ToTable("PlatformUserCLaim", "Security");
            modelBuilder.Entity<PlatformUserRole>().ToTable("PlatformUserRole", "Security");
            modelBuilder.Entity<PlatformUserLogin>().ToTable("PlatformUserLogin", "Security");
            modelBuilder.Entity<PlatformRoleClim>().ToTable("PlatformRoleClim", "Security");
            modelBuilder.Entity<PlatformUserToken>().ToTable("PlatformUserToken", "Security");


            #endregion
        }


    }
}