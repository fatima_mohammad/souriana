﻿using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
   public class BrandCategoryInterface : SharedRepositry, IBrandCategoryInterface
    {
        public BrandCategoryInterface(SourianaMarketContext context) :
         base(context)
        {
        }
    }
}
