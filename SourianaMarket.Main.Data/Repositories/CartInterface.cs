﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.CartDTO;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
    public class CartInterface : SharedRepositry, ICartInterface
    {
        public CartInterface(SourianaMarketContext context) :
         base(context)
        {
        }
        public IEnumerable<CartDto> GetAll()
        {
            return Context.Carts.Select(cart =>new CartDto()
            {
                //ItemPrice = cart.PriceItem,
                Id = cart.Id,
                ItemName = cart.Item.Name,
               
                Quantity = cart.Quantity,
                ItemImageUrl= cart.Item.ImageUrl
                //Categid=country.Item.
                //CountryCode = country.CurrencyCode
            });
           
        }

        public OperationResult SetCart(CartDto model)
        {
            try
            {
                var setCart = Context.Carts.SingleOrDefault(estate => estate.Id == model.Id);
                //**
                var entityState = EntityState.Modified;
                if (setCart == null)
                {
                    setCart = new CartSet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }

                setCart.Quantity = model.Quantity;
                setCart.ItemId = model.ItemId;
                //setCart.PriceItem = model.ItemPrice;

                Context.Entry(setCart).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }

        }
        public bool DeleteCart(int Id)
        {
            try
            {
                var entityState = EntityState.Modified;

                var delCart = Context.Carts.SingleOrDefault(cart =>
                    cart.Id == Id);
                delCart.DeleteAt = DateTimeOffset.Now;
                Context.Entry(delCart).State = entityState;
                var cart = Context.Carts.SingleOrDefault(cart =>
                    cart.Id == Id && !cart.DeleteAt.HasValue);
                Context.Carts.Remove(cart);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteAll()
        {
            try
            {
                var rows = from CartSet in Context.Carts select CartSet;
                foreach (var row in rows)
                {
                    Context.Carts.Remove(row);
                }
               
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
