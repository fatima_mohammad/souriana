﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
    public class BillInterface : SharedRepositry, IBillInterface
    {
        public BillInterface(SourianaMarketContext context) :
           base(context)
        {
        }

        public IEnumerable<BillDto> GetAll(int userid)
        {

            var bill = Context.Bills.Include(ww=>ww.BillItems).Where(a => a.State == true&&a.BillItems.All(wwq=>wwq.UserSetId==userid))
             .Select(
             bill => new BillDto()
             {
                 BillId = bill.Id,
                 Date = bill.Date
             });
            return bill;

        }
        //}

        public IEnumerable<BillDto> GetAllitem()
        {
            return Context.BillItems.Select(bill => new BillDto()
            {
                BillItemId = bill.Id,
                BillId = bill.BillId,
                ItemId = bill.ItemId,
                Quantity = bill.Quantity,
                Userid = bill.UserSetId,
                ItemName = bill.Item.Name,
                ItemPrice = bill.OldPrice,
                ItemImageUrl = bill.Item.ImageUrl,
                Date = bill.Bill.Date,
                Username = bill.UserSet.Name
            });
            //var bill = Context.BillItems
            //  .Select(
            //  bill => new BillDto()
            //  {
            //      BillItemId = bill.Id,
            //      BillId = bill.BillId,
            //      ItemId = bill.ItemId,
            //      Quantity = bill.Quantity,
            //      Userid = bill.UserId,
            //      ItemName = bill.Item.Name,
            //      ItemPrice = bill.OldPrice,
            //      ItemImageUrl = bill.Item.ImageUrl,
            //      Date = bill.Bill.Date,
            //      Username = bill.User.Name
            //  });
            //return bill;

        }

        public OperationResult SetBill(BillDto billDto)
        {
            try
            {
                var setBill = Context.Bills.SingleOrDefault(estate => estate.Id == billDto.BillId);
                //**
                var entityState = EntityState.Modified;
                if (setBill == null)
                {
                    setBill = new BillSet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }
                var billitem = new BillItemSet()
                {
                    BillId = billDto.BillId,
                    ItemId = billDto.ItemId,
                    UserSetId = billDto.Userid,
                    Quantity = billDto.Quantity,
                    //OldPrice = billDto.ItemPrice
                };
                Context.BillItems.Add(billitem);
                Context.SaveChanges();

                setBill.Date = billDto.Date;

                Context.Entry(setBill).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }
        }


        public bool Checkout(int userId)
        {
            var bill = Context.Bills.Where(qq => !qq.State).FirstOrDefault();
            if (bill != null)
            {
                bill.State = true;
                Context.Bills.Update(bill);
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }


    }
}
