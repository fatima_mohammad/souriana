﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.CategoryDTO;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
   public class CategoryInterface : SharedRepositry, ICategoryInterface
    {
        public CategoryInterface(SourianaMarketContext context) :
          base(context)
        {

        }

        public List<CategoryDto> GetAll()
        {
                return Context.Categorys.Select(category => new CategoryDto()
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description,
                    Type = category.Type,
                    ImageUrl = category.ImageUrl
                    //CountryCode = country.CurrencyCode
                }).ToList();
            }

        public CategoryDto GetItemCategory(int itemId)
        {
            return Context.Items.Where(ww=>ww.Id==itemId).Select(category => new CategoryDto()
            {
                Id = category.BrandCategory.CategoryId,
                Name = category.BrandCategory.Category.Name,
                Description = category.BrandCategory.Category.Description,
                Type = category.BrandCategory.Category.Type,
                ImageUrl = category.BrandCategory.Category.ImageUrl
                //CountryCode = country.CurrencyCode
            }).FirstOrDefault();
        }

        public OperationResult SetCategory(CategoryDto model)
        {
            try
            {
                var setCategory = Context.Categorys.SingleOrDefault(estate => estate.Id == model.Id);
                //**
                var entityState = EntityState.Modified;
                if (setCategory == null)
                {
                    setCategory = new CategorySet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }
                setCategory.Name = model.Name;
                setCategory.Description = model.Description;
                setCategory.Type = model.Type;
                setCategory.ImageUrl = model.ImageUrl;

                Context.Entry(setCategory).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }
        }
        public bool DeleteCategory(int Id)
        {
            try
            {
                var entityState = EntityState.Modified;

                var delCategory = Context.Categorys.SingleOrDefault(category =>
                    category.Id == Id);
                delCategory.DeleteAt = DateTimeOffset.Now;
                Context.Entry(delCategory).State = entityState;
                var category = Context.Categorys.SingleOrDefault(category =>
                    category.Id == Id && !category.DeleteAt.HasValue);
                Context.Categorys.Remove(category);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public CategoryDto GetCategoryById(int id)
        {
            var editeView = Context.Categorys.Where(estate => estate.Id == id && !estate.DeleteAt.HasValue)
               .Select(
               estate => new CategoryDto()
               {
                   Id = estate.Id,
                   Name = estate.Name,
                   Description = estate.Description,
                   Type = estate.Type,
                   //ImageUrl = estate.ImageUrl
               }).SingleOrDefault();
            return editeView;

        }
        public bool EditAddCategory(CategoryDto categorySet)
        {
            try
            {
                var EditAddCategory = Context.Categorys.SingleOrDefault(category =>
                    category.Id == categorySet.Id && !category.DeleteAt.HasValue);


                var entityState = EntityState.Modified;
                if (EditAddCategory== null)
                {
                    EditAddCategory = new CategorySet();
                    entityState = EntityState.Added;
                }
                EditAddCategory.Name = categorySet.Name;
                EditAddCategory.Description = categorySet.Description;
                EditAddCategory.Type = categorySet.Type;
                if (!string.IsNullOrEmpty(categorySet.ImageUrl))
                {
                    EditAddCategory.ImageUrl = categorySet.ImageUrl;
                }
                //EditAddCategory.ImageUrl = categorySet.ImageUrl;
                Context.Entry(EditAddCategory).State = entityState;
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
