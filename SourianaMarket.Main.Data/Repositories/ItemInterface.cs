﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.Dto.ItemDTO;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
   public class ItemInterface : SharedRepositry, IItemInterface
    {
        public ItemInterface(SourianaMarketContext context) :
         base(context)
        {
        }

        public ItemDto GetItemById(int id)
        {
            var item = Context.Items.Where(a => a.Id == id && !a.DeleteAt.HasValue)
                .Select(
                a => new ItemDto()
                { 
                    Id=a.Id,
                    Name=a.Name,
                    BrandId=a.BrandCategory.BrandId,
                    CategoryId=a.BrandCategory.CategoryId,
                    BrandCategoryId =a.BrandCategoryId,
                    Description = a.Description,
                    Price = a.Price,
                    Model = a.Model,
                    ImageUrl = a.ImageUrl,
                    Measure = a.Measure,
                    Type = a.Type,
                    RAM = a.RAM,
                    VedioCard = a.VedioCard,
                    Screen = a.Screen,
                    CPU = a.CPU,
                    SSD = a.SSD,
                    HDD = a.HDD,
                    Brandname = a.BrandCategory.Brand.Name,
                    Categoryname = a.BrandCategory.Category.Name,
                    CategoryType = a.BrandCategory.Category.Type,
                    //CategoryId = a.BrandCategory.CategoryId
                }).SingleOrDefault();
            return item;
        }

        public IEnumerable<ItemDto> GetItemsById(int id)
        {
            var editeView = Context.Items.Where(estate => estate.BrandCategory.Category.Id == id && !estate.DeleteAt.HasValue)
               .Select(
               estate => new ItemDto()
               {
                   Id=estate.Id,
                   Name = estate.Name,
                   Description = estate.Description,
                   Price = estate.Price,
                   Model = estate.Model,
                   ImageUrl = estate.ImageUrl,
                   Measure = estate.Measure,
                   Type = estate.Type,
                   RAM = estate.RAM,
                   VedioCard = estate.VedioCard,
                   Screen = estate.Screen,
                   CPU = estate.CPU,
                   SSD = estate.SSD,
                   HDD = estate.HDD,
                   Brandname = estate.BrandCategory.Brand.Name,
                   Categoryname = estate.BrandCategory.Category.Name,
                   CategoryType=estate.BrandCategory.Category.Type,
                   CategoryId=estate.BrandCategory.CategoryId
                   
                   });
                   //.SingleOrDefault();
                return editeView;
        }

        

        public bool DeleteItem(int Id)
        {
            try
            {
                var entityState = EntityState.Modified;

                var delItem = Context.Items.SingleOrDefault(item =>
                    item.Id == Id);
                delItem.DeleteAt = DateTimeOffset.Now;
                Context.Entry(delItem).State = entityState;
                var item = Context.Items.SingleOrDefault(item =>
                    item.Id == Id && !item.DeleteAt.HasValue);
                Context.Items.Remove(item);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EditAddItem(ItemDto itemSet)
        {
            try
            {
                var EditAddItem = Context.Items.SingleOrDefault(item =>
                    item.Id == itemSet.Id && !item.DeleteAt.HasValue);
                

                var entityState = EntityState.Modified;
                var brandCategory = Context.BrandCategorys.
                       Where(ww => ww.CategoryId == itemSet.CategoryId && ww.BrandId == itemSet.BrandId)
                       .FirstOrDefault();
                if (brandCategory is null)
                {
                    brandCategory = new BrandCategorySet()
                    {
                        CategoryId = itemSet.CategoryId,
                        BrandId = itemSet.BrandId
                    };
                    Context.BrandCategorys.Add(brandCategory);
                    Context.SaveChanges();

                }
                if (EditAddItem == null)
                {
                    EditAddItem = new ItemSet();
                   
                    if (brandCategory is null)
                    {
                        brandCategory = new BrandCategorySet()
                        {
                            CategoryId = itemSet.CategoryId,
                            BrandId = itemSet.BrandId
                        };
                        Context.BrandCategorys.Add(brandCategory);
                        Context.SaveChanges();

                    }
                    //EditAddItem = new ItemSet();
                    //EditAddItem.BrandCategoryId = brandCategory.Id;
                    entityState = EntityState.Added;
                }
                EditAddItem.Name = itemSet.Name;
                EditAddItem.BrandCategoryId = brandCategory.Id;
                EditAddItem.Description = itemSet.Description;
                EditAddItem.Price = itemSet.Price;
                EditAddItem.Model = itemSet.Model;
                if (!string.IsNullOrEmpty(itemSet.ImageUrl))
                {
                    EditAddItem.ImageUrl = itemSet.ImageUrl;
                }
                //EditAddItem.ImageUrl = itemSet.ImageUrl;
                EditAddItem.Measure = itemSet.Measure;
                EditAddItem.Type = itemSet.Type;
                EditAddItem.RAM = itemSet.RAM;
                EditAddItem.VedioCard = itemSet.VedioCard;
                EditAddItem.Screen = itemSet.Screen;
                EditAddItem.CPU = itemSet.CPU;
                EditAddItem.SSD = itemSet.SSD;
                EditAddItem.HDD = itemSet.HDD;
               

                //EditAddBrand.ImageUrl = brandSet.ImageUrl;
                Context.Entry(EditAddItem).State = entityState;
                Context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public OperationResult SetItem(ItemDto itemSet)
        {
            try
            {
                var setItem = Context.Items.SingleOrDefault(estate => estate.Id == itemSet.Id);
                //**
                var entityState = EntityState.Modified;
                if (setItem == null)
                {
                    setItem = new ItemSet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }
                var brandCategory = Context.BrandCategorys.
                        Where(ww => ww.CategoryId == itemSet.CategoryId && ww.BrandId == itemSet.BrandId)
                        .FirstOrDefault();
                if (brandCategory is null)
                {
                    brandCategory = new BrandCategorySet()
                    {
                        CategoryId = itemSet.CategoryId,
                        BrandId = itemSet.BrandId
                    };

                    Context.BrandCategorys.Add(brandCategory);
                    Context.SaveChanges();

                }
                //EditAddItem = new ItemSet();
                //EditAddItem.BrandCategoryId = brandCategory.Id;
                entityState = EntityState.Added;
                setItem.BrandCategoryId = brandCategory.Id;
                setItem.Name = itemSet.Name;
                setItem.Description = itemSet.Description;
                setItem.Type = itemSet.Type;
                setItem.Price = itemSet.Price;
                setItem.Measure = itemSet.Measure;
                setItem.RAM = itemSet.RAM;
                setItem.VedioCard = itemSet.VedioCard;
                setItem.Screen = itemSet.Screen;
                setItem.CPU = itemSet.CPU;
                setItem.SSD = itemSet.SSD;
                setItem.HDD = itemSet.HDD;
                setItem.ImageUrl = itemSet.ImageUrl;

                
                Context.Entry(setItem).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }
        }

        //public IEnumerable<ItemDto> GetWomanItem(int id)
        //{
        //    var editeView = Context.Items.Where(estate => estate.BrandCategory.Category.Id == id &&
        //    estate.Type=="Woman" && !estate.DeleteAt.HasValue)
        //     .Select(
        //     estate => new ItemDto()
        //     {
        //         Id = estate.Id,
        //         Name = estate.Name,
        //         Description = estate.Description,
        //         Price = estate.Price,
        //         Model = estate.Model,
        //         ImageUrl = estate.ImageUrl,
        //         Measure = estate.Measure,
        //         Type = estate.Type,
        //         RAM = estate.RAM,
        //         VedioCard = estate.VedioCard,
        //         Screen = estate.Screen,
        //         CPU = estate.CPU,
        //         SSD = estate.SSD,
        //         HDD = estate.HDD,
        //         Brandname = estate.BrandCategory.Brand.Name,
        //         Categoryname = estate.BrandCategory.Category.Name,
        //         CategoryType = estate.BrandCategory.Category.Type,

        //     });
        //    //.SingleOrDefault();
        //    return editeView;
        //}

        public List<ItemDto> Search(string term)
        {
            var result = Context.Items.Where(b => b.Name.Contains(term) || b.Description.Contains(term)
            || b.BrandCategory.Brand.Name.Contains(term)|| b.Measure.Contains(term) || b.Type.Contains(term)
            || b.Model.Contains(term)|| b.RAM.Contains(term) || b.VedioCard.Contains(term) ||
            b.Screen.Contains(term)|| b.CPU.Contains(term)|| b.SSD.Contains(term)||b.HDD.Contains(term)
            ).Select(estate => new ItemDto()
            {
                Id = estate.Id,
                Name = estate.Name,
                Description = estate.Description,
                Price = estate.Price,
                Model = estate.Model,
                ImageUrl = estate.ImageUrl,
                Measure = estate.Measure,
                Type = estate.Type,
                RAM = estate.RAM,
                VedioCard = estate.VedioCard,
                Screen = estate.Screen,
                CPU = estate.CPU,
                SSD = estate.SSD,
                HDD = estate.HDD,
                Brandname = estate.BrandCategory.Brand.Name,
                Categoryname = estate.BrandCategory.Category.Name,
                CategoryType = estate.BrandCategory.Category.Type,
                CategoryId = estate.BrandCategory.CategoryId
            }).ToList();
            return result;
        }
    }
}
