﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.BrandDTO;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
  public  class BrandInterface : SharedRepositry, IBrandInterface
    {
        public BrandInterface(SourianaMarketContext context) :
           base(context)
        {
        }

        public IEnumerable<BrandDto> GetAll()
        {
            return Context.Brands.Select(country => new BrandDto()
            {
                Id = country.Id,
                Name = country.Name,
                Description = country.Description,
                Location=country.Location,
                ImageUrl=country.ImageUrl
                //CountryCode = country.CurrencyCode
            });
        }

       

        public BrandDto GetBrandById(int id)
        {
            var editeView = Context.Brands.Where(estate => estate.Id == id && !estate.DeleteAt.HasValue)
               .Select(
               estate => new BrandDto()
               {
                   Id = estate.Id,
                   Name = estate.Name,
                   Description = estate.Description,
                   Location = estate.Location,
                   //ImageUrl = estate.ImageUrl
               }).SingleOrDefault();
            return editeView;

        }

        public OperationResult SetBrand(BrandDto model)
        {
            try
            {
                var setBrand = Context.Brands.SingleOrDefault(estate => estate.Id == model.Id);
                //**
                var entityState = EntityState.Modified;
                if (setBrand == null)
                {
                    setBrand = new BrandSet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }
                setBrand.Name = model.Name;
                setBrand.Description = model.Description;
                setBrand.Location = model.Location;
                if (!string.IsNullOrEmpty(model.ImageUrl))
                {
                    setBrand.ImageUrl = model.ImageUrl;
                }


                Context.Entry(setBrand).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }
        }
        public bool DeleteBrand(int Id)
        {
            try
            {
                var entityState = EntityState.Modified;

                var delBrand = Context.Brands.SingleOrDefault(brand =>
                    brand.Id == Id);
                delBrand.DeleteAt = DateTimeOffset.Now;
                Context.Entry(delBrand).State = entityState;
                var brand = Context.Brands.SingleOrDefault(brand =>
                    brand.Id == Id && !brand.DeleteAt.HasValue);
                Context.Brands.Remove(brand);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
    

        public bool EditAddBrand(BrandDto brandSet)
        {
            try
            {
                var EditAddBrand = Context.Brands.SingleOrDefault(brand =>
                    brand.Id == brandSet.Id && !brand.DeleteAt.HasValue);


                var entityState = EntityState.Modified;
                if (EditAddBrand == null)
                {
                    EditAddBrand = new BrandSet();
                    entityState = EntityState.Added;
                }
                EditAddBrand.Name = brandSet.Name;
                EditAddBrand.Description =brandSet.Description;
                EditAddBrand.Location = brandSet.Location;
                if (!string.IsNullOrEmpty(brandSet.ImageUrl))
                {
                    EditAddBrand.ImageUrl = brandSet.ImageUrl;
                }
                Context.Entry(EditAddBrand).State = entityState;
                Context.SaveChanges();
             
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }



    }
}
