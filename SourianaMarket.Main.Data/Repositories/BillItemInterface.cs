﻿using Microsoft.EntityFrameworkCore;
using SourianaMarket.Main.Data.SharedBase;
using SourianaMarket.Main.Dto.BillDTO;
using SourianaMarket.Main.Dto.BillItemDTO;
//using SourianaMarket.Main.Dto.BillItemDTO;
using SourianaMarket.Main.Dto.Generic;
using SourianaMarket.Main.IData.Interfaces;
using SourianaMarket.Models.Commerce;
using SourianaMarket.SharedKarnel.Enums;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SourianaMarket.Main.Data.Repositories
{
   public class BillItemInterface : SharedRepositry, IBillItemInterface
    {
        public BillItemInterface(SourianaMarketContext context) :
          base(context)
        {
        }

        public bool DeleteBillItem(int Id,int userid)
        {
            try
            {
                var entityState = EntityState.Modified;

                var delBillItem = Context.BillItems.SingleOrDefault(billitem =>
                    billitem.Id == Id);
                delBillItem.DeleteAt = DateTimeOffset.Now;
                Context.Entry(delBillItem).State = entityState;
                var billitem = Context.BillItems.SingleOrDefault(billitem =>
                    billitem.Id == Id &&billitem.UserSetId==userid &&!billitem.DeleteAt.HasValue);
                Context.BillItems.Remove(billitem);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public IEnumerable<BillItemDto> GetAll(int userid)
        {

            var billitem = Context.BillItems.Where(a => a.UserSetId == userid&&!a.Bill.State)
               .Select(
               billitem => new BillItemDto()
               {
                   Id = billitem.Id,
                   ItemName = billitem.Item.Name,
                   Quantity = billitem.Quantity,
                   ItemImageUrl = billitem.Item.ImageUrl,
                   ItemPrice = billitem.Item.Price,
                   BillId = billitem.Bill.Id,
                   UserId = billitem.UserSet.Id,
               });
            return billitem;

        }

        public OperationResult SetBillItem(BillItemDto billitemDto)
        {
            try
            {
                var setBillitem = Context.BillItems.SingleOrDefault(estate => estate.Id == billitemDto.Id);
                //**
                var entityState = EntityState.Modified;
                if (setBillitem == null)
                {
                    setBillitem = new BillItemSet();
                    entityState = EntityState.Added;
                    //**
                    // setBrand.Views = setBrand.Views;
                }

                var statebill = Context.Bills.
                    Where(ww => ww.State == false )
                    .FirstOrDefault();
                if(statebill is null)
                {
                    statebill = new BillSet()
                    {
                        Date = DateTime.Today,
                        State=false
                    };
                    Context.Bills.Add(statebill);
                    Context.SaveChanges();

                }
                

                setBillitem.ItemId = billitemDto.ItemId;
                setBillitem.BillId = statebill.Id;
                setBillitem.Quantity = billitemDto.Quantity;
                setBillitem.OldPrice = billitemDto.OldPrice;
                setBillitem.UserSetId = billitemDto.UserId; 
                Context.Entry(setBillitem).State = entityState;
                Context.SaveChanges();
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.Success,
                    
                };
            }
            catch (Exception ex)
            {
                return new OperationResult()
                {
                    IsSuccess = true,
                    ResultType = OperationResultTypes.failed,
                    ExceptionMessage = ex,
                };
            }
        }
        public bool DeleteAll(int userid)
        {
            try
            {
                var rows = from BillItemSet in Context.BillItems select BillItemSet;
                foreach (var row in rows)
                {
                    Context.BillItems.Remove(row);
                }
                var billitem = Context.BillItems.
                       Where(ww => ww.Bill.State == false && ww.UserSetId==userid)
                       .FirstOrDefault();
                Context.BillItems.Remove(billitem);
                Context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
