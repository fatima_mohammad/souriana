﻿using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Main.Data.SharedBase
{
  public  class SharedRepositry
    {
        protected SourianaMarketContext Context { get; }

        public SharedRepositry(SourianaMarketContext context)
        {
            Context = context;
        }
    }
}
