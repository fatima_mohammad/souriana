﻿using SourianaMarket.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SourianaMarket.Security
{
    public  interface IAccountRepository
    {

       
        AccountDto GetAccount(int id);
         Task <AccountDto> AciotnAccount(AccountDto accountDto);
        Task<AccountDto> HttpLogin(AccountDto loginDto);    
        Task<bool> HttpLogout();
         bool CheckUserNameAvilabilty(string userName);
        int GetUserId();
     


    }
}
