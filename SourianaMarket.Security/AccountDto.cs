﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SourianaMarket.Security
{
   public class AccountDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string lastName { get; set; }
        public string Info { get; set; }
        public bool IsValid { get; set; }
        public string Email { get; set; }
   
        public string PhoneNumber { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool RemeberMe { get; set; }
        public int OrderCount { get; set; }



    }
}
