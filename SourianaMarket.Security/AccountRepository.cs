﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using SourianaMarket.Models.Commerce;
using SourianaMarket.Models.Security;
using SourianaMarket.Models.User;
using SourianaMarket.Security;
using SourianaMarket.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SourianaMarket.Security
{
    public class AccountRepository : IAccountRepository
    {
        public SignInManager<PlatformUser> SignInManager { get; }
        public RoleManager<PlatformRole> RoleManger { get; }
        public UserManager<PlatformUser> UserManager { get; }
        public HttpContext HttpContext { get; set; }
        public SourianaMarketContext Context { get; set; }
        public AccountRepository(SourianaMarketContext context, RoleManager<PlatformRole> roleManager,
            UserManager<PlatformUser> userManager,
            SignInManager<PlatformUser> signInManager,
            HttpContext httpContext)
        {
            Context = context;
            RoleManger = roleManager;
            UserManager = userManager;
            SignInManager = signInManager;
            HttpContext = httpContext;
        }
        public async Task<AccountDto> AciotnAccount(AccountDto accountDto)
        {
            try
            {
                var currentUser = await UserManager.FindByNameAsync(accountDto.UserName);
                if (currentUser == null)
                {


                    currentUser = new UserSet()
                    {
                        ImageUrl = accountDto.ImageUrl,
                        Name = accountDto.FirstName,
                        Email = accountDto.Email,
                        UserName = accountDto.UserName,
                        PhoneNumber = accountDto.PhoneNumber,

                    };
                    var result = await UserManager.CreateAsync(currentUser, accountDto.Password);

                    if (!result.Succeeded)
                    {
                        return new AccountDto();
                    }
                    var role = await RoleManger.FindByIdAsync(accountDto.RoleId.ToString());
                    if (role != null)
                    {
                        var roleresult = await UserManager.AddToRoleAsync(currentUser, role.Name);
                    }
                    accountDto.Id = currentUser.Id;
                    accountDto.RoleName = role.Name;
                    return accountDto;
                }

                else
                {
                    var user = Context.Users.Where(ww => ww.Id == currentUser.Id).SingleOrDefault();
                    currentUser.Info = string.IsNullOrEmpty(accountDto.Info) ? currentUser.Info : accountDto.Info;
                    currentUser.ImageUrl = accountDto.ImageUrl;
                    currentUser.Name = accountDto.FirstName;
                    currentUser.Email = accountDto.Email;
                    currentUser.UserName = accountDto.UserName;
                    currentUser.PhoneNumber = accountDto.PhoneNumber;
                    currentUser.Info = accountDto.Info;
                    var result = await UserManager.UpdateAsync(currentUser);
                    var scT = await UserManager.UpdateSecurityStampAsync(currentUser);
                    var role = await RoleManger.FindByIdAsync("3");
                    if (!string.IsNullOrEmpty(role.Name))
                    {
                        var roleresult = await UserManager.AddToRoleAsync(currentUser, role.Name);
                        accountDto.RoleName = role.Name;
                        accountDto.Id = currentUser.Id;
                    }
                    return accountDto;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserNameAvilabilty(string userName)
        {
            var user = Context.Users.Where(freelancer => freelancer.UserName == userName).ToList();
            if (user.Count() != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public AccountDto GetAccount(int id)
        {
            var userpp = Context.Users.Where(fd => fd.Id == id).SingleOrDefault();


            var account = new AccountDto
            {
                Id = userpp.Id,
                Email = userpp.Email,
                ImageUrl = userpp.ImageUrl,
                Info = userpp.Info,
                FirstName = userpp.Name,
                UserName = userpp.UserName,
                RoleId = 2,
            };
            return account;
        }

        public int GetUserId()
        {
            try
            {
                if (HttpContext != null
                    && HttpContext.User != null
                    && HttpContext.User.Identity != null
                    && HttpContext.User.Identity.IsAuthenticated)
                {
                    return int.Parse(HttpContext.User
                        .FindFirstValue(ClaimTypes.NameIdentifier));
                }
            }
            catch (Exception ex)
            {

            }
            return 0;

        }

        public async Task<AccountDto> HttpLogin(AccountDto loginDto)
        {
            try
            {

                var userEntity = await UserManager.FindByNameAsync(loginDto.UserName);
                var result = await SignInManager.CheckPasswordSignInAsync(userEntity, loginDto.Password, false);

                if (userEntity == null || !result.Succeeded)
                {
                    return new AccountDto();
                }
                if (!result.Succeeded)
                {
                    loginDto.Id = userEntity.Id;
                    return loginDto;
                }
                var sdfdf = await SignInManager.PasswordSignInAsync(userEntity, loginDto.Password, true, false);

                var userRoles = await UserManager.GetRolesAsync(userEntity) as List<string>;

                loginDto.Id = userEntity.Id;
                return new AccountDto()
                {
                    Id = userEntity.Id,
                    Email = userEntity.Email,
                    UserName = userEntity.UserName,
                    RoleName = userRoles[0]
                };
            }
            catch (Exception ex)
            {
                return new AccountDto();
            }
        }

        public async Task<bool> HttpLogout()
        {
            try
            {
                await SignInManager.SignOutAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }


}
